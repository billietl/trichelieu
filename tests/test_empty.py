from unittest import TestCase
from unittest.mock import Mock, MagicMock
import random
import trichelieu


class TestEmptyCalls(TestCase):

    def setUp(self):
        self.bot = trichelieu.Bot()
        self.bot.add_spam_caller(".*", Mock())
        self.bot.add_emoji_caller(".*", "1")
        self.bot.user.mentioned_in = MagicMock(return_value=False)
        self.message = Mock()
        self.message.content = "Un message !"
        random.randint = MagicMock(return_value=0)

    def test_when_random_is_bad_and_no_override_then_no_message(self):
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 0)

    def test_when_random_is_bad_and_no_override_then_no_emoji(self):
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 0)


if __name__ == '__main__':
    unittest.main()
