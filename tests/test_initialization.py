from unittest import TestCase
import trichelieu


class TestInitialization(TestCase):

    def test_default_token_is_empty(self):
        mybot = trichelieu.Bot()
        self.assertEqual(mybot.token, '')

    def test_default_spam_functions_is_empty(self):
        mybot = trichelieu.Bot()
        self.assertEqual(mybot.spam_functions, {})

    def test_default_emoji_functions_is_empty(self):
        mybot = trichelieu.Bot()
        self.assertEqual(mybot.emoji_functions, {})

    def test_passed_token_is_registered(self):
        mybot = trichelieu.Bot(token='mytoken')
        self.assertEqual(mybot.token, 'mytoken')

    def test_passed_spam_rate_is_registered(self):
        mybot1 = trichelieu.Bot(spam_rate=1)
        mybot2 = trichelieu.Bot(spam_rate=2)
        self.assertEqual(mybot1.spam_rate, 1)
        self.assertEqual(mybot2.spam_rate, 2)

    def test_passed_emoji_rate_is_registered(self):
        mybot1 = trichelieu.Bot(emoji_rate=1)
        mybot2 = trichelieu.Bot(emoji_rate=2)
        self.assertEqual(mybot1.emoji_rate, 1)
        self.assertEqual(mybot2.emoji_rate, 2)


if __name__ == '__main__':
    unittest.main()
