from unittest import TestCase
from unittest.mock import Mock, PropertyMock, MagicMock
import random
import trichelieu


class TestCalls(TestCase):

    def setUp(self):
        self.bot = trichelieu.Bot()
        self.bot.add_spam_caller(".*", Mock())
        self.bot.add_emoji_caller(".*", "1")
        self.message = Mock()
        self.message.content = "Un message !"
        self.message.guild.emojis = []

    def test_when_message_from_me_then_no_response(self):
        type(self.bot).user = PropertyMock()
        self.message.author = self.bot.user
        random.randint = MagicMock(return_value=1)
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 0)

    def test_when_random_is_good_then_message(self):
        random.randint = MagicMock(return_value=1)
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)

    def test_when_random_is_good_then_emoji(self):
        random.randint = MagicMock(return_value=1)
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)

    def test_when_random_is_bad_and_override_then_message(self):
        self.bot.user.mentioned_in = MagicMock(return_value=True)
        random.randint = MagicMock(return_value=0)
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)

    def test_when_random_is_bad_and_override_then_emoji(self):
        self.bot.user.mentioned_in = MagicMock(return_value=True)
        random.randint = MagicMock(return_value=0)
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)


if __name__ == '__main__':
    unittest.main()
