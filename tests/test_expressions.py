from unittest import TestCase
from unittest.mock import Mock, MagicMock
import time
import os
import re
import random
import main
import trichelieu


class TestExpressionSpam(TestCase):

    def setUp(self):
        self.bot = trichelieu.Bot()
        self.message = Mock()
        random.randint = MagicMock(return_value=1)

        for regex, func in main.spams.items():
            self.bot.add_spam_caller(regex, func)

    def test_perdu_uses_display_name(self):
        self.message.content = "J'ai perdu"
        self.message.author.display_name = "Gérard"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci Gérard, à cause de toi j'ai perdu !")

    def test_perdu0(self):
        self.message.content = "J'ai perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu1(self):
        self.message.content = "t'as perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu2(self):
        self.message.content = "tu as perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu3(self):
        self.message.content = "il a perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu4(self):
        self.message.content = "elle a perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu5(self):
        self.message.content = "on a perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu6(self):
        self.message.content = "nous avons perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu7(self):
        self.message.content = "vous avez perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu8(self):
        self.message.content = "ils ont perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_perdu9(self):
        self.message.content = "elles ont perdu"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Pas merci toto, à cause de toi j'ai perdu !")

    def test_faim1(self):
        self.message.content = "j'ai faim"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "*jette une frite à toto*")

    def test_faim2(self):
        self.message.content = "g faim"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "*jette une frite à toto*")

    def test_perceval1(self):
        self.message.content = "c'est pas faux"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "T'as pas compris quoi, toto ?")

    def test_perceval2(self):
        self.message.content = "c'est pas fau"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "T'as pas compris quoi, toto ?")

    def test_perceval3(self):
        self.message.content = "c pa fo"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "T'as pas compris quoi, toto ?")

    def test_perceval4(self):
        self.message.content = "C'est juste que c'est pas forcément les"
        self.message.author.display_name = "toto"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 0)

    def test_perceval_uses_display_name(self):
        self.message.content = "c'est pas faux"
        self.message.author.display_name = "Gérard"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "T'as pas compris quoi, Gérard ?")

    def test_dad_joke1(self):
        self.message.content = "je suis grand"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0],
            "Bonjour grand, moi c'est Trichelieu")

    def test_dad_joke2(self):
        self.message.content = "je suis "
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(responses[0], "Bonjour , moi c'est Trichelieu")

    def test_dad_joke3(self):
        self.message.content = "je pense, donc, je suis"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 0)

    def test_hello_there1(self):
        self.message.content = "hello there"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(responses[0], "General Kenobi")

    def test_rrrrr1(self):
        self.message.content = "ça va être tout noir"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(responses[0], "ta gueule")

    def test_rrrrr2(self):
        self.message.content = "ca va etre tout noir"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(responses[0], "ta gueule")

    def test_heure1(self):
        self.message.content = "quel heure ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0], "IL EST .* DU MATIN ! TOUT VA BIEN")

    def test_heure2(self):
        self.message.content = "quelle heure ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0], "IL EST .* DU MATIN ! TOUT VA BIEN")

    def test_heure3(self):
        self.message.content = "quel heure est-il ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0], "IL EST .* DU MATIN ! TOUT VA BIEN")

    def test_heure4(self):
        self.message.content = "quelle heure est-il ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0], "IL EST .* DU MATIN ! TOUT VA BIEN")

    def test_heure_non_consistent(self):
        self.message.content = "quelle heure est-il ?"
        os.environ['TZ'] = "Europe/London"
        time.tzset()
        responses1 = self.bot.compute_message_response(self.message)
        os.environ['TZ'] = "Europe/Paris"
        time.tzset()
        responses2 = self.bot.compute_message_response(self.message)
        self.assertNotEqual(responses1[0], responses2[0])

    def test_tki1(self):
        self.message.content = "qui es-tu ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')

    def test_tki2(self):
        self.message.content = "qui est-tu ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')

    def test_tki3(self):
        self.message.content = "qui es tu ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')

    def test_tki4(self):
        self.message.content = "qui est tu ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')

    def test_tki5(self):
        self.message.content = "t'es qui ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')

    def test_tki6(self):
        self.message.content = "t ki"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')

    def test_tki7(self):
        self.message.content = "mais au fait, qui es-tu ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')

    def test_tki8(self):
        self.message.content = "@Trichelieu_test qui es-tu ?"
        responses = self.bot.compute_message_response(self.message)
        self.assertEqual(len(responses), 1)
        self.assertEqual(
            responses[0], 'Je suis Trichelieu ! '
            'Clerc de Raïa, heu, de Travia ! '
            'Si tu veux que je me mut, il te suffit de dire '
            '"Je suis satisfait(e) de mes soins" !')


class TestExpressionEmoji(TestCase):

    def setUp(self):
        self.bot = trichelieu.Bot()
        self.message = Mock()
        self.message.guild.emojis = []
        random.randint = MagicMock(return_value=1)

        for regex, emoji in main.emojis.items():
            self.bot.add_emoji_caller(regex, emoji)
            e = Mock()
            e.name = emoji
            self.message.guild.emojis.append(e)

    def test_nice1(self):
        self.message.content = "nice"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0], "👌")

    def test_poulpy1(self):
        self.message.content = "poulpy"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "poulpy")

    def test_dalc1(self):
        self.message.content = "dalc"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "logo")

    def test_dalc2(self):
        self.message.content = "des a la carte"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "logo")

    def test_dalc2(self):
        self.message.content = "dés à la carte"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "logo")

    def test_bitenboa1(self):
        self.message.content = "bite"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "Bitenboa")

    def test_bitenboa3(self):
        self.message.content = "cul"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "Bitenboa")

    def test_bitenboa5(self):
        self.message.content = "couille"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "Bitenboa")

    def test_chatbrosse1(self):
        self.message.content = "chat"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "ChatBrosse")

    def test_chatbrosse2(self):
        self.message.content = "minou"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "ChatBrosse")

    def test_chatbrosse3(self):
        self.message.content = "gratouille"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "ChatBrosse")

    def test_chatbrosse4(self):
        self.message.content = "caresse"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "ChatBrosse")

    def test_chatbrosse5(self):
        self.message.content = "miaou"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "ChatBrosse")

    def test_chatbrosse6(self):
        self.message.content = "maou"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "ChatBrosse")

    def test_chatbrosse7(self):
        self.message.content = "pat"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "ChatBrosse")

    def test_chatbrosse8(self):
        self.message.content = "je mange des pates carbo"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 0)

    def test_fakenews1(self):
        self.message.content = "je suis mauvais"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "fake")

    def test_fakenews2(self):
        self.message.content = "tu es perdant"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "fake")

    def test_fakenews3(self):
        self.message.content = "la taverne oubliée est géniale"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "fake")

    def test_fakenews4(self):
        self.message.content = "nous sommes des nains"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "fake")

    def test_fakenews5(self):
        self.message.content = "vous êtes incompris"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "fake")

    def test_fakenews6(self):
        self.message.content = "ils sont ici"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "fake")

    def test_fakenews7(self):
        self.message.content = "j'aimes les pommes"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 0)

    def test_fakenews8(self):
        self.message.content = "c'est comestible"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "fake")

    def test_fakenews9(self):
        self.message.content = "il en est où ?"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 0)

    def test_salty1(self):
        self.message.content = "fait chier"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "salty")

    def test_salty2(self):
        self.message.content = "d'la merde"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "salty")

    def test_salty3(self):
        self.message.content = "putain d'sa race"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "salty")

    def test_salty4(self):
        self.message.content = "c nul"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 1)
        self.assertRegex(responses[0].name, "salty")

    def test_salty5(self):
        self.message.content = "j'aimes les pommes"
        responses = self.bot.compute_message_emoji(self.message)
        self.assertEqual(len(responses), 0)


if __name__ == '__main__':
    unittest.main()
