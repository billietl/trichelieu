from unittest import TestCase
import re
import main


class TestRegex(TestCase):

    def test_emoji_regex_are_valid(self):
        for regex, emoji in main.emojis.items():
            re.compile(regex)

    def test_spam_regex_are_valid(self):
        for regex, func in main.spams.items():
            re.compile(regex)

    def test_emoji_regex_are_not_catchall(self):
        for regex, emoji in main.emojis.items():
            result = re.match(regex, "")
            self.assertEqual(result, None)

    def test_spam_regex_not_catchall(self):
        for regex, func in main.spams.items():
            result = re.match(regex, "")
            self.assertEqual(result, None)


if __name__ == '__main__':
    unittest.main()
