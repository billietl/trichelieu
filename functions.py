import re
import time


def rrrrr(message):
    return "ta gueule"


def hello_there(message):
    return "General Kenobi"


def dad_joke(message):
    matches = re.match(".*je suis (.*)", message.content, re.IGNORECASE)
    if matches is not None:
        adj = matches.group(1).split(" ")[0]
    else:
        adj = ""
    return "Bonjour {}, moi c'est Trichelieu".format(adj)


def perceval(message):
    return "T'as pas compris quoi, {} ?".format(
        message.author.display_name)


def faim(message):
    return "*jette une frite à {}*".format(
        message.author.display_name)


def perdu(message):
    return "Pas merci {}, à cause de toi j'ai perdu !".format(
        message.author.display_name)


def heure(message):
    heure = time.localtime()
    return 'IL EST {} HEURES {} DU MATIN ! TOUT VA BIEN !'.format(
        heure.tm_hour, heure.tm_min)


def tki(message):
    heure = time.localtime()
    return 'Je suis Trichelieu ! Clerc de Raïa, heu, de Travia ! ' \
        'Si tu veux que je me mut, il te suffit de dire ' \
        '"Je suis satisfait(e) de mes soins" !'
