FROM python:3.7-slim
# run via docker run -e TZ="Europe/Paris" -e token="your token here" --name trichelieu trichelieu

CMD python3 main.py

RUN mkdir /home/trichelieu

WORKDIR /home/trichelieu

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . ./
