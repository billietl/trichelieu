import asyncio
import os
import random
import re
import inspect
import discord


class Bot(discord.Client):

    def __init__(self, token='', spam_rate=50, emoji_rate=15):
        super().__init__()
        self.spam_functions = {}
        self.emoji_functions = {}
        self.token = token
        self.spam_rate = spam_rate
        self.emoji_rate = emoji_rate

    def add_spam_caller(self, regex, func):
        self.spam_functions[regex] = func

    def add_emoji_caller(self, regex, func):
        self.emoji_functions[regex] = func

    async def on_ready(self):
        print("Logged in as {} ({}) on following servers :".format(
            self.user.name, self.user.id))
        for s in self.guilds:
            print("  {}".format(s))
        print("-----")

    async def on_message(self, message):
        for reponse in self.compute_message_response(message):
            print("# Sending on {}'s channel {} : {}".format(
                str(message.channel.guild),
                str(message.channel),
                reponse))
            await message.channel.send(reponse)
        for reponse in self.compute_message_emoji(message):
            print("# Adding reaction {} on {}'s channel {}".format(
                reponse,
                str(message.channel.guild),
                str(message.channel)))
            await message.add_reaction(reponse)

    def compute_message_response(self, message):
        responses = []
        if message.author != self.user:
            resultat_de_spam = random.randint(1, int(self.spam_rate))
            if resultat_de_spam == 1 or self.user.mentioned_in(message):
                for regex, func in self.spam_functions.items():
                    if (re.match(regex, message.content, re.IGNORECASE)):
                        responses.append(func(message))
        return responses

    def compute_message_emoji(self, message):
        responses = []
        resultat_de_emoji = random.randint(1, int(self.emoji_rate))
        if resultat_de_emoji == 1 or self.user.mentioned_in(message):
            for regex, func in self.emoji_functions.items():
                if (re.match(regex, message.content, re.IGNORECASE)):
                    if len(func) == 1:
                        responses.append(func)
                    else:
                        emoji_id = discord.utils.get(
                            message.guild.emojis,
                            name=func)
                        if emoji_id is not None:
                            responses.append(emoji_id)
        return responses

    def run(self):
        super().run(self.token)
