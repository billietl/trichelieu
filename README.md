# louisbilliet/trichelieu:latest
`Dockerfile` to create a [Docker](http://www.docker.com) container image running an annoying discord bot.

## Contributing
Pull requests on branch `testing` are welcome.

To add a response, add a new function in the `functions.py` file.
That function must take 1 parameter being a [message](https://discordpy.readthedocs.io/en/latest/api.html#message) and return a `string` the bot should post on the same channel.

Then, in the `main.py` file, add an entry in the `spams` hash : the key is the regular expression that should trigger the bot, and the value is the function to call (it should be the function you added in the `functions.py` file).

To add a reaction, add an entry in the `emojis` hash : the key is the regular expression that should trigger the bot, and the value is either the custom emoji name in the server the bot is connected to (without the colons) or the unicode caracter to use.

The bot's new responses and reactions must be unit-tested in `tests/test_expressions.py`.
All un-tested new reactions won't be accepted.

A gitlab-CI pipeline is set up on this project.
It will take care of :
- running unit tests
- running code-style check
- build the docker image
- run the bot for 5 seconds on a test server
- deploy the bot

In order to test the bot, you can join [the test server](https://discord.gg/qTYWkXH), where the bot is set up to react every time.

# Getting started
## Installation
This is a standard python project, and you should use virtualenv to stay clean :
```bash
pip install virtualenv
virtualenv .
pip install -r requirements.txt
pip install -r requirements_tests.txt
```
## Quickstart
Once your virtualenv is set up, run the bot locally with this command :
```bash
TZ="<Your timezone>" token="<Your bot token here>" SPAM_RATE="<the higher, the fewer>" EMOJI_RATE="<the higher, the fewer>" python main.py
```
You now have a running discord bot.
