#!/usr/bin/python

import trichelieu
import os
import inspect

import functions

emojis = {
    ".*(chat|minou|gratouille|caresse|mi?aou).*": "ChatBrosse",
    ".*pat(\\s+.*)?$": "ChatBrosse",
    ".*(bite|cul|couille).*": "Bitenboa",
    ".*(dalc|d(e|é)s (a|à) la carte).*": "logo",
    ".*poulpy.*": "poulpy",
    ".*nice.*": '\N{OK HAND SIGN}',
    ".*(c'|\\s)(suis|est?|somm?es|(ê|e)tes|sont)\\s[^\\?]*$": 'fake',
    ".*(chier|merde|putain|nul).*": 'salty'
}

spams = {
    ".*(c|ç)a va (e|ê)tre tout noir.*": functions.rrrrr,
    "^hello there$": functions.hello_there,
    ".*je suis (.*)": functions.dad_joke,
    "(c('est)? pa(s)? f(aux?|o))( .*|$)": functions.perceval,
    ".*j'ai perdu.*": functions.perdu,
    ".*(tu as|t'as) perdu.*": functions.perdu,
    ".*(il|elle|on) a perdu.*": functions.perdu,
    ".*nous avons perdu.*": functions.perdu,
    ".*vous avez perdu.*": functions.perdu,
    ".*(il|elle)s ont perdu.*": functions.perdu,
    ".*quel(le)? heure.*\\?.*": functions.heure,
    ".*(j'ai|g) faim.*": functions.faim,
    ".*((t('est?)? (qu|k)i)|((qu|k)i est?(-| )tu)).*": functions.tki
}

if __name__ == "__main__":
    myBot = trichelieu.Bot(
        os.getenv('token', ''),
        os.getenv('SPAM_RATE', 50),
        os.getenv('EMOJI_RATE', 15)
    )

    for regex, func in spams.items():
        myBot.add_spam_caller(regex, func)
    for regex, emoji in emojis.items():
        myBot.add_emoji_caller(regex, emoji)

    myBot.run()
